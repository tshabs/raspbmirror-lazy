#!/bin/bash

# install packages
yum install epel-release nginx centos-release-scl rh-python36 yum-cron -y

# setup web directory
mkdir -p /home/www/$1/html
chown -R nginx:nginx /home/www/$1/html

# setup nginx
/bin/cp _conf/raspbian.conf /etc/nginx/conf.d/raspbian.conf
sed -i "s/##HOSTNAME##/$1/g" /etc/nginx/conf.d/raspbian.conf

# setup yum-cron
sed -i "s/apply_updates = no/apply_updates = yes/g" /etc/yum/yum-cron.conf

# setup python 3.6 environment
scl enable rh-python36 "python -m venv /home/www/$1/py3env >/dev/null 2>&1"

wget -N https://github.com/plugwash/raspbian-tools/raw/master/raspbmirror.py -P /home/www/$1
mkdir /home/www/$1/tmp

/bin/cp mirror.sh /home/www/$1/

systemctl enable yum-cron
systemctl enable nginx

echo "=======SETUP COMPLETE======="
echo "You should now run: "
echo "./mirror.sh whatever.yourdomain.com"
