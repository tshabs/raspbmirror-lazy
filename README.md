README
======

Quick & Lazy script to setup a raspian mirror on centos 7


Setup
-----
Run as root
```
./setup.sh whatever.yourdomain.com
```

First Run
---------
This might take a while for initial sync
```
cd /home/www/$1/html
/home/www/$1/py3env/bin/python3 ../raspbmirror.py --tmpdir ../tmp
```
